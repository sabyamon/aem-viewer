aem-viewer
==========

AEM Viewer is an extension which helps developers and testers to quickly check information about a page which is hosted in Adobe AEM/Day CQ . 

Use cases for AEM Viewer .

The result has been split into two views , first tab shows the RAW data and the second tab shows the Skimmed Data . 

**** Intention is to show data in a non geeky way in the "Skimmed Data" tab and dumping the raw data in the "RAW Data" tab.

For Developers :

1. Quickly Debug a AEM/CQ Page . (To debug in Author Environment, please remove the content finder 'cf#' from the url ) .
2. To know what are the components a CQ page is made of .
3. Check who has created , modified and activated the page without going to CRXDE .
4. Checking Page Properties in Publisher .



For Testers :

1. Content Validation for a particular component.
2. Testing a component.
3. Check the template and page component of a particular page .

** This extension is intended to use in Author and Publish environments and not through dispatcher. 

Soon , I will add an update link from which you can take update for this extension.

Feedbacks are always welcome to make this extension more useful.



